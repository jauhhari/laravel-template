@extends('admin.master')
@section('content')
    <div class="fluid-container">
        <div class="col-md-12 mt-5">
            <div class="card">
                <div class="card-body">
                    <h5>Selamat datang di halaman dashboard, <strong>{{ Auth::user()->name }}</strong></h5>
                    <a href="{{ route('logout') }}" class="btn btn-danger">Logout</a>
                </div>
            </div>
        </div>
    </div>
@endsection
