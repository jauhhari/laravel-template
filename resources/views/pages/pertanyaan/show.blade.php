@extends('admin.master')
@section('content')
<div class="content-header d-flex flex-column flex-md-row mb-3">
    <div class="row">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb mb-0">
                <li class="breadcrumb-item" aria-current="page"><a href="/pertanyaan">Semua pertanyaan</a></li>
                <li class="breadcrumb-item active" aria-current="page">{{$pertanyaan->judul}}</li>
            </ol>
        </nav>
    </div>
</div>
<div class="row">
    <div class="col">
        <div class="card">
            <div class="card-body">
                <form method="POST" action="{{route('pertanyaan.update', $pertanyaan->id)}}">
                    @csrf
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Judul</label>
                            <input required name="judul" value="{{$pertanyaan->judul}}" type="text" class="form-control">
                        </div>
                        <div class="form-group">
                            <label>Isi</label>
                            <input required name="isi" value="{{$pertanyaan->isi}}" type="text" class="form-control">
                        </div>
                    </div>
                    <button type="submit" name="_method" value="put" class="btn btn-primary">Ubah Data</button>
                    <button type="submit" name="_method" value="delete" class="btn btn-danger">Hapus</button>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
